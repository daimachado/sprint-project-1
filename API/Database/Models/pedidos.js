const mongoose = require('mongoose');
const {config} = require ('dotenv');
const { DB_URL } = process.env;

//const URL = 'mongodb+srv://daiana:Trinidad@cluster0.vypcc.mongodb.net/API-Delilah?retryWrites=true&w=majority';
const URL = DB_URL
mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

const pedidosPagoSchema = {
    idPedido: Number,
    hora: { type: Date, default: Date.now }, //new Date(),
    idUsuario: Number,
    usuario: String,
    idProductos: Array,//[],
    estado: String,
    direccionEnvioPedido: String,
    medioPago: Object,  //[],
    costoTotal: Number,
    cerrado: {type:Boolean, default:false},
};

const Pedidos = mongoose.model('Pedidos', pedidosPagoSchema);


const pedido1 = {
    idPedido: 1,
    hora: new Date(),
    idUsuario: 2,
    usuario: "juan_machado",
    idProductos: [],
    estado: "Pendiente",
    direccionEnvioPedido: 'Magallanes',
    medioPago: [],
    costoTotal: 0,
    cerrado: false,
};
const pedido2 = {
    idPedido: 2,
    hora: new Date(),
    idUsuario: 2,
    usuario: "juan_machado",
    idProductos: [],
    estado: "Pendiente",
    direccionEnvioPedido: 'Magallanes',
    medioPago: [],
    costoTotal: 0,
    cerrado: false,
};

let nuevo_pedido1 = new Pedidos(pedido1);
let nuevo_pedido2 = new Pedidos(pedido2);

nuevo_pedido1.save();
nuevo_pedido2.save();

//mongoose.connection.once('open', () => console.log('Conectando con la base de datos'));

module.exports = {
    Pedidos
}