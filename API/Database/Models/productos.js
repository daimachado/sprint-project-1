const mongoose = require('mongoose');
const {config} = require ('dotenv');
const { DB_URL } = process.env;

const URL = DB_URL

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

const productosSchema = {
        idProducto: Number,
        producto: String,
        precio: Number,
        activo: {type:Boolean, default:true},
};

const Productos = mongoose.model('Productos', productosSchema);

const producto1 = {
        idProducto: 1,
        producto: 'Baguel de salmón',
        precio: 425,
        activo: true
};
const producto2 = {
        idProducto: 2,
        producto: 'Hamburguesa clásica',
        precio: 350,
        activo: true
};
const producto3 = {
        idProducto: 3,
        producto: 'Sandwich veggie',
        precio: 310,
        activo: true
};

const producto4 = {
        idProducto: 4,
        producto: 'Ensalada veggie',
        precio: 340,
        activo: true
};

let nuevo_producto1 = new Productos(producto1);
let nuevo_producto2 = new Productos(producto2);
let nuevo_producto3 = new Productos(producto3);
let nuevo_producto4 = new Productos(producto4);

nuevo_producto1.save();
nuevo_producto2.save();
nuevo_producto3.save();
nuevo_producto4.save();

// mongoose.connection.once('open', () => console.log('Conectando con la base de datos'));

module.exports = {

        Productos
}