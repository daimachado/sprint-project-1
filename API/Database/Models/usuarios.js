const { config } = require('dotenv');
const mongoose = require('mongoose');
const { encriptacion } = require('../../Middleware/encriptacion');
config();
const {DB_URL}= process.env;
mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

const usuarioSchema = {
    idUsuario: Number,
    soyAdmin: { type: Boolean, default: false },
    usuario: { type: String, unique: true }, //usuario: String,
    nombreApellido: String,
    mail: { type: String, unique: true }, //mail: String, //Hay forma de verificar mail?
    password: String,
    telefono: Number,
    agendaDirecciones: Array, //direccionEnvio: String,
    idProductosHistorial: Object,
    logueado: { type: Boolean, default: false }, //antes registrado
    suspendido: { type: Boolean, default: false },
};

const Usuarios = mongoose.model('Usuarios', usuarioSchema);

const admin = {
    idUsuario: 1,
    soyAdmin: true,
    usuario: "dai",
    nombreApellido: "Daiana Machado",
    mail: "machadodaianag@gmail.com",
    password: encriptacion("123456"),
    telefono: 2901123456,
    //direccionEnvio: "Magallanes 9410",
    agendaDirecciones: [],
    idProductosHistorial: [],
    //logueado: true
};

const admin2 = {
    idUsuario: 2,
    soyAdmin: true,
    usuario: "juan",
    nombreApellido: "Juan Machado",
    mail: "machadojuan@gmail.com",
    password: encriptacion("123456"),
    telefono: 2901654321,
    //direccionEnvio: "Magallanes 9410",
    agendaDirecciones: [],
    idProductosHistorial: [],
    //registrado: false
};

const usuario1 = {
    idUsuario: 3,
    //soyAdmin: false,
    usuario: "trini",
    nombreApellido: "Trini B",
    mail: "trini@gmail.com",
    password: encriptacion("123456"),
    telefono: 2901654321,
    //direccionEnvio: "Magallanes 9410",
    agendaDirecciones: [],
    idProductosHistorial: [],
    //logueado: true
};

const usuario2 = {

    idUsuario: 4,
    soyAdmin: false,
    usuario: "pedro",
    nombreApellido: "Pedro B",
    mail: "pedro@gmail.com",
    password: encriptacion("123456"),
    telefono: 2901654321,
    //direccionEnvio: "Güemes 150",
    agendaDirecciones: [],
    idProductosHistorial: [],
    //registrado: false

};

async function usuariosPrueba(usuariosPrueba) {
    try {
        const nuevoUsuario = new Usuarios(usuariosPrueba);
        await nuevoUsuario.save();
    }
    catch {
        (e) => {
            console.log(e);
        }
    }
};

usuariosPrueba(admin);
usuariosPrueba(admin2);
usuariosPrueba(usuario1);
usuariosPrueba(usuario2);

/*function mostrarLosUsuarios (){
    const todosLosUsuarios = Usuarios.find();
    todosLosUsuarios.then((x)=>{console.log(x);})
};
mostrarLosUsuarios()
Usuarios.find().then(function (resultados){
    console.log(resultados);
});

*/

module.exports = {
    Usuarios
}

//let nuevo_admin1 = new Usuarios(admin);
//let nuevo_admin2= new Usuarios (admin2);
//let nuevo_usuario1=new Usuarios(usuario1);
//let nuevo_usuario2=new Usuarios(usuario2);

//nuevo_admin1.save();
//nuevo_admin2.save();
//nuevo_usuario1.save();
//nuevo_usuario2.save();


//mongoose.connection.once('open', () => console.log('Conectando con la base de datos'));
