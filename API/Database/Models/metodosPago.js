const mongoose = require('mongoose');
const {config} = require ('dotenv');
const { DB_URL } = process.env;

const URL = DB_URL

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });

const metodosPagoSchema = {
    idPago : Number, 
    metodoPago : String,
    detalle : String,
    activo : {type:Boolean, default:true}
};

const MetodosPago = mongoose.model('Métodos de pago', metodosPagoSchema);


const metodoPago1 =  {
        idPago: 1,
        metodoPago: 'Efectivo',
        detalle: 'Que va aca?',
        activo: true
    };
const metodoPago2 = {
        idPago: 2,
        metodoPago: 'Debito',
        detalle: 'VISA',
        activo: true
    };

let nuevo_metodoPago1 = new MetodosPago (metodoPago1);
let nuevo_metodoPago2 = new MetodosPago (metodoPago2);

nuevo_metodoPago1.save();
nuevo_metodoPago2.save();

//mongoose.connection.once('open', () => console.log('Conectando con la base de datos'));

module.exports = {
    MetodosPago
}