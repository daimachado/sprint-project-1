const e = require("express");
const { MetodosPago } = require("../Database/Models/metodosPago");

/*function MetodosPago(idPago, metodoPago, detalle, activo) {
        this.idPago = idPago,
        this.metodoPago = metodoPago,
        this.detalle = detalle,
        this.activo = true
};
*/
/* let metodosPago = [
    {
        idPago: 1,
        metodoPago: 'Efectivo',
        detalle: 'Que va aca?',
        activo: true
    },
    {
        idPago: 2,
        metodoPago: 'Debito',
        detalle: 'VISA',
        activo: true
    },
    {
        idPago: 3,
        metodoPago: 'Credito',
        detalle: 'VISA',
        activo: false
    }
];
*/
async function mostrarMetodosPago(peticion, respuesta) {
    await MetodosPago.find({}).then((metodosPago)=>{     
        return respuesta.status(200).json(metodosPago);    
    })
};

async function nuevoMetodoPago (peticion, respuesta){
    const metodosPago = await MetodosPago.find();
    let nuevoMetodoPago = new MetodosPago();
    nuevoMetodoPago.metodoPago = peticion.body.metodoPago;
    nuevoMetodoPago.detalle = peticion.body.detalle;
    nuevoMetodoPago.idPago = metodosPago[metodosPago.length - 1].idPago + 1;
    if (nuevoMetodoPago.metodoPago && nuevoMetodoPago.detalle) {
        await nuevoMetodoPago.save().then((x)=>{     
            let mensaje = `El nuevo método de pago ${nuevoMetodoPago.metodoPago}/${nuevoMetodoPago.detalle} se ha incorporado con éxito.`;
            return respuesta.status(200).json(mensaje);    
        })
    } else {
        let mensaje = `Para incorporar el nuevo método de pago debe completar todos los campos.`;
        respuesta.status(401).json(mensaje);
    }
};

/* function editarMetodosPago(peticion, respuesta){
    const pagoID = Number(peticion.params.id);
    for (const pagos of metodosPago) {
        if (pagoID === pagos.idPago) {
            if (pagos.activo === false) {
                pagos.activo = true;
                let mensaje = `Has modificado el estado de tu método de pago a Activo.`;
                respuesta.status(200).json(mensaje);
            } else {
                pagos.activo = false;
                let mensaje = `Has modificado el estado de tu método de pago a Inactivo.`;
                respuesta.status(200).json(mensaje);
            }
        } 
    }
    let mensaje = `No se ha encontrado tu método de pago. Intente nuevamente.`;
    respuesta.status(404).json(mensaje);
}; */

async function editarMetodosPago(peticion, respuesta){
    const pagoID = Number(peticion.params.id);
    await MetodosPago.findOne({idPago:pagoID}).then((pagoModificado)=>{   
        if (pagoModificado.activo === false) {
            pagoModificado.activo = true;
            let mensaje = `Has modificado el estado de tu método de pago a Activo.`;
            respuesta.status(200).json(mensaje);
        } else {
            pagoModificado.activo = false;
            let mensaje = `Has modificado el estado de tu método de pago a Inactivo.`;
            respuesta.status(200).json(mensaje);
        }
        return console.log(pagoModificado);
        }) 
    .catch((e)=>{
        let mensaje = `No se ha encontrado tu método de pago. Intente nuevamente.`;
        respuesta.status(404).json(mensaje);
    })
};

/*function borrarMetodoPago(peticion, respuesta) {
    const pagoID = Number(peticion.params.id);
    for (const pagos of metodosPago) {
        if (pagoID === pagos.idPago) {
            metodosPago.splice(metodosPago.indexOf(pagos), 1);
            let mensaje = `El método de pago ${metodosPago.metodosPago} ha sido eliminado.`;
            return respuesta.status(200).json(mensaje);
        } 
    }
    let mensaje = `El método de pago no ha sido encontrado.`;
    respuesta.status(404).json(mensaje);
};*/ 

async function borrarMetodoPago(peticion, respuesta) {
    const pagoID = Number(peticion.params.id);
    await MetodosPago.findOneAndDelete({idPago:pagoID }).then((metodoEliminado)=>{     
    let mensaje = `El método de pago ${metodoEliminado.metodosPago} ha sido eliminado.`;
    return respuesta.status(200).json(mensaje);    
    })
    .catch((e)=>{     
        let mensaje = `El método de pago no ha sido encontrado.`;
        return respuesta.status(200).json(mensaje);    
        })
}; // CAMBIAR findOneByIdAndDelete( const: _id ) 


module.exports = {
    //metodosPago,
    nuevoMetodoPago,
    mostrarMetodosPago,
    editarMetodosPago,
    borrarMetodoPago
};