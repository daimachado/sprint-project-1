/* function Productos (idProducto, producto, precio, activo) {
    this.idProducto = idProducto,
    this.producto = producto,
    this.precio = precio
    this.activo = true
}; */

const { Productos } = require("../Database/Models/productos");
const redis = require ("redis");
const { error } = require("console");

/* let productos = [
    {
        idProducto: 1,
        producto: 'Baguel de salmón',
        precio: 425,
        activo: true
    },
    {
        idProducto: 2,
        producto: 'Hamburguesa clásica',
        precio: 350,
        activo: true
    },
    {
        idProducto: 3,
        producto: 'Sandwich veggie',
        precio: 310,
        activo: true
    },
    {
        idProducto: 4,
        producto: 'Ensalada veggie',
        precio: 340,
        activo: true
    },
    {
        idProducto: 5,
        producto: 'Focaccia',
        precio: 300,
        activo: true
    },
    {
        idProducto: 6,
        producto: 'Sandwich Focaccia',
        precio: 440,
        activo: true
    }

]; */

/*function mostrarProductos (peticion, respuesta){
    respuesta.json(productos);
    console.log (productos)
};*/


//const client = redis.createClient();
const client = redis.createClient({
    host: "127.0.0.1",
    port: 6379,
});
client.on("error", function (error) {
    console.error(error);
});

async function conectarRedis (){
    await client.connect();
}
conectarRedis();

async function mostrarProductos(peticion, respuesta) {
    try{
        //await client.connect();
        const ping = await client.ping();
        console.log(ping);
        client.get("productos", (error, rep) =>{
            if (error){
                respuesta.json(error);
            }
            if (rep) {
                console.log(rep);
                respuesta.json(rep);
            }
        });
        const productoRedis = await Productos.find ();
        //console.log(productoRedis);
        client.set("productos", JSON.stringify(productoRedis));
        return respuesta.status(200).json(productoRedis);
    } catch {
        respuesta.status(404).json("No se han podido cargar los productos")
    }
};

/*
    await Productos.find({}).then((productos) => {
        return respuesta.status(200).json(productos);
    })
*/

async function nuevoProducto(peticion, respuesta) {
    const productos = await Productos.find();
    let nuevoProducto = new Productos();
    nuevoProducto.producto = peticion.body.producto;
    nuevoProducto.precio = Number(peticion.body.precio);
    nuevoProducto.idProducto = productos[productos.length - 1].idProducto + 1;
    if (nuevoProducto.producto && nuevoProducto.precio) {
        await nuevoProducto.save().then((x) => {
            let mensaje = `El nuevo producto ${nuevoProducto.producto} se ha incorporado con éxito`;
            respuesta.status(200).json(mensaje)
        })
    } else {
        let mensaje = `Para incorporar el nuevo producto debe completar todos los campos`;
        respuesta.status(401).json(mensaje);
    }
    //await client.connect();
    const productoRedis = await Productos.find ();
    client.set("productos", JSON.stringify(productoRedis));
};

async function editarProducto(peticion, respuesta) {
    const productoID = Number(peticion.params.id);
    await Productos.findOne({ idProducto: productoID }).then((productoModificado) => {
        productoModificado.producto = peticion.body.producto;
        productoModificado.precio = peticion.body.precio;
        productoModificado.save();
        let mensaje = `Has modificado el nombre de tu producto.`;
        respuesta.status(200).json(mensaje);
    })
        .catch((e) => {
            let mensaje = `No se ha encontrado el producto. Intente nuevamente.`;
            respuesta.status(404).json(mensaje);

        })
        //await client.connect();
        const productoRedis = await Productos.find ();
        client.set("productos", JSON.stringify(productoRedis));
};

/* function borrarProducto (peticion, respuesta){
    const idProductoBorrado = Number(peticion.params.id);
    for (const producto of productos) {
        if (idProductoBorrado === producto.idProducto) {
            const posicionProducto = productos.indexOf(producto);
            productos.splice(posicionProducto, 1);
            return respuesta.status(200).send(`Producto ${producto} ha sido eliminado.`);
        }
    }
    let mensaje = `El producto no ha sido encontrado, intente nuevamente.`;
    respuesta.status(404).json(mensaje);
};*/

async function borrarProducto(peticion, respuesta) {
    const idProductoBorrado = Number(peticion.params.id);
    await Productos.findOneAndDelete({ idProducto: idProductoBorrado }).then((productoBorrado) => {
        let mensaje = `El producto${productoBorrado.producto} ha sido eliminado.`;
        return respuesta.status(200).json(mensaje);
    })
        .catch((e) => {
            let mensaje = `El producto no ha sido encontrado, intente nuevamente.`;
            return respuesta.status(200).json(mensaje);
        })
}; // CAMBIAR findOneByIdAndDelete( const: _id ) 

module.exports = {
    nuevoProducto,
    mostrarProductos,
    editarProducto,
    borrarProducto
};