const { Pedidos } = require("../Database/Models/pedidos");
const { Productos } = require("../Database/Models/productos");
const { Usuarios } = require("../Database/Models/usuarios");
const mongoose = require("mongoose");
const BD = mongoose.connection;

/* function Pedidos(idPedido, hora, idUsuario, usuario, idProductos, estado, direccionEnvioPedido, medioPago, costoTotal, cerrado) {
    this.idPedido = idPedido,
        this.hora = new Date(),
        this.idUsuario = idUsuario,
        this.usuario = usuario,
        this.idProductos = [],
        this.estado = estado,
        this.direccionEnvioPedido = direccionEnvioPedido,
        this.medioPago = [],
        this.costoTotal = costoTotal,
        this.cerrado = false
};
*/

/* let pedidos = [
    {
        idPedido: 1,
        hora: new Date(),
        idUsuario: 2,
        usuario: "juan_machado",
        idProductos: [],
        estado: "Pendiente",
        direccionEnvioPedido: 'Magallanes',
        medioPago: [],
        costoTotal: 0,
        cerrado: false,
    }, {
        idPedido: 2,
        hora: new Date(),
        idUsuario: 2,
        usuario: "juan_machado",
        idProductos: [],
        estado: "Pendiente",
        direccionEnvioPedido: 'Magallanes',
        medioPago: [],
        costoTotal: 0,
        cerrado: false,
    }, {
        idPedido: 3,
        hora: new Date(),
        idUsuario: 2,
        usuario: "juan_machado",
        idProductos: [],
        estado: "Pendiente",
        direccionEnvioPedido: 'Magallanes',
        medioPago: [],
        costoTotal: 0,
        cerrado: false,
    }
]; */

const estado = [

    {
        idEstado: 1,
        estado: 'Pendiente'
    }, {
        idEstado: 2,
        estado: 'Confirmado'
    },
    {
        idEstado: 3,
        estado: 'En preparación'
    },
    {
        idEstado: 4,
        estado: 'Enviado / En camino'
    },
    {
        idEstado: 5,
        estado: 'Entregado'
    },
    {
        idEstado: 6,
        estado: 'Cancelado'
    }
];

/*function mostrarPedidos(peticion, respuesta) {
    respuesta.json(pedidos);
    console.log(pedidos)
};*/

async function mostrarPedidos(peticion, respuesta) {
    await Pedidos.find({}).then((pedidos) => {
        return respuesta.status(200).json(pedidos);
    })
};

async function iniciaPedido(peticion, respuesta) {
    const pedidos = await Pedidos.find();
    const usuarioID = Number(peticion.headers.id);
    const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
    let nuevoPedido = new Pedidos();
    nuevoPedido.idUsuario = usuarioBD.idUsuario;
    nuevoPedido.idProductos = [];
    nuevoPedido.estado = "Pendiente";
    nuevoPedido.costoTotal = 0;
    nuevoPedido.usuario = usuarioBD.usuario;
    nuevoPedido.idPedido = pedidos[pedidos.length - 1].idPedido + 1;
   /*  if (pedidos.length === 0) {
        nuevoPedido.idPedido = 1
    } else {
        nuevoPedido.idPedido = pedidos[pedidos.length - 1].idPedido + 1;
    }; */
    await nuevoPedido.save().then((x) => {
        let mensaje = `${nuevoPedido.usuario} iniciaste tu pedido ${nuevoPedido.idPedido} . Ahora vamos a llenar ese carrito!`;
        respuesta.status(200).json(mensaje);
    })
};

async function agregarProducto(peticion, respuesta) {
    try {
        const usuarioID = Number(peticion.headers.id);
        const pedidoID = Number(peticion.params.id);
        const productoID = Number(peticion.params.in);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        const pedidoBD = await Pedidos.findOne({ idPedido: pedidoID });
        const productoBD = await Productos.findOne({ idProducto: productoID });
        const sesion = await BD.startSession();
        sesion.startTransaction();
        if (usuarioBD.idUsuario === pedidoBD.idUsuario) {
            //if(pedidoBD.cerrado === false){
            const loqueagregamo = productoBD
            /* {
                //cantidad: Number(peticion.body.cantidad),
                idProductos: productoBD
            } */
            pedidoBD.idProductos.push(loqueagregamo);
            pedidoBD.costoTotal = (pedidoBD.costoTotal) + (productoBD.precio); // pedido.montoPago += producto.precio * productoAgregado.cantidad;
            await pedidoBD.save();
            await sesion.commitTransaction();
            let mensaje = `Un delicioso ${productoBD.producto} se ha agregado a su pedido. Seguí sumando cosas ricas...`;
            respuesta.status(200).json(mensaje);
        } else {
            await sesion.abortTransaction();
            let mensaje = `Ya no es posible modificar su pedido... Realice un nuevo pedido o comuniquese con nuestro operador.`
            respuesta.status(403).json(mensaje)
        }
        sesion.endSession();
    } catch {
        let mensaje = `No encontramos ese producto, pero no dejes de probar con otras cosas ricas de Delilah.`;
        return respuesta.status(404).json(mensaje);
    }
};

/* function agregarProducto(peticion, respuesta) {
    let usuarioID = Number(peticion.headers.id);
    let pedidoID = Number(peticion.params.id);
    let productoID = Number(peticion.params.in);
    for (const usuario of usuarios) {
        if (usuarioID === usuario.idUsuario) {
            for (const pedido of pedidos) {
                if (pedidoID === pedido.idPedido) {
                    for (const producto of productos) {
                        if (productoID === producto.idProducto) {
                            pedido.idProductos.push(producto);
                            pedido.costoTotal = (pedido.costoTotal) + (producto.precio);
                            console.log(pedido.costoTotal);
                            let mensaje = `Un delicioso ${producto.producto} se ha agregado a su pedido. Seguí sumando cosas ricas...`;
                            return respuesta.status(200).json(mensaje);
                        }
                    }
                }
            }
        }
    }
    let mensaje = `No encontramos ese producto, pero no dejes de probar con otras cosas ricas de Delilah.`;
    respuesta.status(404).json(mensaje);
}; */


async function sacarProducto(peticion, respuesta) {
    try {
        const usuarioID = Number(peticion.headers.id);
        const pedidoID = Number(peticion.params.id);
        const productoID = Number(peticion.params.in);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        const pedidoBD = await Pedidos.findOne({ idPedido: pedidoID });
        const productoBD = await Productos.findOne({ idProducto: productoID });
        const sesion = await BD.startSession();
        sesion.startTransaction();
        if (usuarioBD.idUsuario === pedidoBD.idUsuario) {
            //console.log("HOLA");
            for (const producto of pedidoBD.idProductos) {
                if (producto.idProducto === productoID) {
                    pedidoBD.costoTotal = (pedidoBD.costoTotal) - (productoBD.precio); // pedido.montoPago += producto.precio * productoAgregado.cantidad;
                    //pedidoBD.idProductos.splice(pedidoBD.idProductos.indexOf(productoID), 1);
                    const loquesacamo = pedidoBD.idProductos.filter((producto) => producto.idProducto !== productoID);
                    pedidoBD.idProductos.length = 0;
                    pedidoBD.idProductos = loquesacamo;
                    //console.log(pedidoBD);
                    await pedidoBD.save();
                    //console.log(pedidoBD);
                    await sesion.commitTransaction();
                    let mensaje = `El ${productoBD.producto} ha sido retirado de tu pedido, probá con otras cosas ricas de Delilah.`;
                    return respuesta.status(200).json(mensaje);
                };
            }
        } else {
            await sesion.abortTransaction();
            let mensaje = `Ese producto no está en tu pedidooo`;
            return respuesta.status(403).json(mensaje);
        }
        sesion.endSession();
    } catch (error) {
        console.log(error);
        let mensaje = `Ese producto no está en tu pedido`;
        return respuesta.status(404).json(mensaje);
    }
}; // Cuando voy borrando, siempre me deja uno!!!!!!!!!!!!

/* function sacarProducto(peticion, respuesta) {
    let usuarioID = Number(peticion.headers.id);
    let pedidoID = Number(peticion.params.id);
    let productoID = Number(peticion.params.in);
    for (const usuario of usuarios) {
        if (usuarioID === usuario.idUsuario) {
            for (const pedido of pedidos) {
                if (pedidoID === pedido.idPedido) {
                    for (const producto of pedido.idProductos) { // Aca ponerle un IF para validar que hay algo en el carro
                        if (productoID === producto.idProducto) {
                            pedido.costoTotal = (pedido.costoTotal) - (producto.precio);
                            pedido.idProductos.splice(pedido.idProductos.indexOf(producto), 1);
                            console.log(pedido.costoTotal);
                            let mensaje = `El ${producto.producto} ha sido retirado de tu pedido, probá con otras cosas ricas de Delilah.`;
                            return respuesta.status(200).json(mensaje);
                        }
                    }
                    let mensaje = `Ese producto no está en tu pedido`;
                    return respuesta.status(404).json(mensaje);
                }
            }
        }
    }
}; */

/* function pagarPedido(peticion, respuesta) {
    let usuarioID = Number(peticion.headers.id);
    let pedidoID = Number(peticion.params.id);
    for (const pedido of pedidos) {
        if (pedidoID === pedido.idPedido) {
            pedido.cerrado = true;
            pedido.estado = 'Confirmado';
            //for (const producto of pedido.idProductos) {
              // pedido.costoTotal += producto.precio;
                for (const usuario of usuarios) {
                    if (usuarioID === usuario.idUsuario) {
                        usuario.idProductosHistorial.push(pedido.idProductos);
                    }
                }
            //}
 
        }
 
    }
 
    let mensaje = `Su pedido ha sido pagado con éxito. Gracias por pedir a Delilah. Puedes seguir tu pedido para saber donde esta`;
    return respuesta.status(200).json(mensaje)
 
};
*/
// for (const usuario of usuarios) {
//  if (usuarioID === usuario.idUsuario) {
//  usuario.idProductosHistorial.push(pedido.idProductos);
//}
// }

async function pagarPedido(peticion, respuesta) {
    try {
        //console.log("por acaaa");
        const usuarioID = Number(peticion.headers.id);
        const pedidoID = Number(peticion.params.id);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        const pedidoBD = await Pedidos.findOne({ idPedido: pedidoID });
        //console.log("holis");
        pedidoBD.cerrado = true;
        pedidoBD.estado = 'Confirmado';
        await pedidoBD.save();
        const palHistorial = pedidoBD.idProductos;
        //await usuarioBD.idProductosHistorial.push(palHistorial);
        usuarioBD.idProductosHistorial = palHistorial;
        await usuarioBD.save();
        let mensaje = `Su pedido ha sido pagado con éxito. Gracias por pedir a Delilah. Puedes seguir tu pedido para saber donde esta`;
        return respuesta.status(200).json(mensaje)
    } catch {
        (e) => {
            let mensaje = `El pedido no se pudo pagar.`;
            return respuesta.status(404).json(mensaje);
        }
    }

}; //Tengo que chequear si se guardan los productos en el historial...


/* async function pagarPedido(peticion, respuesta) {
    const usuarioID = Number(peticion.headers.id);
    const pedidoID = Number(peticion.params.id);
    await Pedidos.findOne({ idPedido: pedidoID }).then(async (pedidoEncontrado) => {
        console.log("kkkkk");
        pedidoEncontrado.cerrado = true;
        pedidoEncontrado.estado = 'Confirmado';
        await pedidoEncontrado.save();
        console.log("holiiis");
        await Usuarios.findOne({ idUsuario: usuarioID }).then(async (usuario) => {
            const palHistorial = pedidoEncontrado.idProductos;
            await usuario.idProductosHistorial.push(palHistorial).then((x) => {
                let mensaje = `Su pedido ha sido pagado con éxito. Gracias por pedir a Delilah. Puedes seguir tu pedido para saber donde esta`;
                return respuesta.status(200).json(mensaje)
            })
        })
    })
        .catch((e) => {
            let mensaje = `El pedido no se pudo pagar.`;
            return respuesta.status(404).json(mensaje);
        })
}; //Tengo que chequear si se guardan los productos en el historial... */

/*function estadoPedidos(peticion, respuesta) {
    const pedidoID = Number(peticion.body.idPedido);
    for (const pedido of pedidos) {
    if (pedidoID === pedido.idPedido) {
            if (pedido.estado !== "Entregado") {
            pedido.estado = peticion.body.cambioEstado;
            let mensaje = `El pedido ${pedidoID} ha cambiado su estado a ${pedido.estado}.`;
            return respuesta.status(200).json(mensaje);
        } else {
            let mensaje = `Cuando el estado está Entregado no se puede modificar`;
            return respuesta.status(406).json(mensaje);
        }
    } 
        
    
}
let mensaje = `El pedido no ha sido encontrado`;
return respuesta.status(405).json(mensaje);
 
}; */

async function estadoPedidos(peticion, respuesta) {
    const pedidoID = Number(peticion.body.idPedido);
    await Pedidos.findOne({ idPedido: pedidoID }).then(async (pedido) => {
        pedido.estado = peticion.body.estado;
        if (pedido.estado !== "Entregado") {
            pedido.estado = peticion.body.cambioEstado;
            let mensaje = `El pedido ${pedidoID} ha cambiado su estado a ${pedido.estado}.`;
            await pedido.save()
            return respuesta.status(200).json(mensaje);
        } else {
            let mensaje = `Cuando el estado está Entregado no se puede modificar`;
            return respuesta.status(406).json(mensaje);
        }
    })
        .catch((e) => {
            let mensaje = `El pedido no ha sido encontrado`;
            return respuesta.status(405).json(mensaje);
        })

}; //No me anda el IF de si esta entregado no se puede modificar

/*function borrarPedidos(peticion, respuesta) {
    const pedidoID = Number(peticion.params.id);
    for (const pedido of pedidos) {
        if (pedidoID === pedido.idPedido) {
            const ubicacionPedido = pedidos.indexOf(pedido);
            pedidos.splice(ubicacionPedido, 1);
            let mensaje = `El pedido ${pedidoID} ha sido eliminado`;
            return respuesta.status(200).json(mensaje);
        }
    }
    let mensaje = `El pedido ${pedidoID} no ha sido encontrado`;
    respuesta.status(404).json(mensaje);
};*/

async function borrarPedidos(peticion, respuesta) {
    const pedidoID = Number(peticion.params.id);
    await Pedidos.findOneAndDelete({ idPedido: pedidoID }).then((pedidoBorrado) => {
        let mensaje = `El pedido ${pedidoBorrado.idPedido} ha sido eliminado.`;
        return respuesta.status(200).json(mensaje);
    })
        .catch((e) => {
            let mensaje = `El pedido ${pedidoID} no ha sido encontrado`;
            return respuesta.status(200).json(mensaje);
        })
}; // CAMBIAR findOneByIdAndDelete( const: _id ) 

module.exports = {
    Pedidos,
    iniciaPedido,
    mostrarPedidos,
    agregarProducto,
    pagarPedido,
    sacarProducto,
    estadoPedidos,
    borrarPedidos
};