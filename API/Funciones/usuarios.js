const { Usuarios } = require("../Database/Models/usuarios");
const mongoose = require("mongoose");
const { encriptacion } = require("../Middleware/encriptacion");
const e = require("express");
const BD = mongoose.connection;

/*
function Usuarios(idUsuario, soyAdmin, usuario, nombreApellido, mail, password, telefono, direccionEnvio, idProductosHistorial) {
    this.idUsuario = idUsuario,
        this.soyAdmin = false,
        this.usuario = usuario,
        this.nombreApellido = nombreApellido,
        this.mail = mail,
        this.password = password,
        this.telefono = telefono,
        this.direccionEnvio = direccionEnvio,
        this.idProductosHistorial = [],
        this.registrado = false
};
*/
/* let usuarios = [
    {
        idUsuario: 1,
        soyAdmin: true,
        usuario: "dai_machado",
        nombreApellido: "Daiana Machado",
        mail: "machadodaianag@gmail.com",
        password: "123456",
        telefono: 2901123456,
        direccionEnvio: "Magallanes 9410",
        idProductosHistorial: [],
        registrado: true
    }, {
        idUsuario: 2,
        soyAdmin: true,
        usuario: "juan_machado",
        nombreApellido: "Juan Machado",
        mail: "machadojuan@gmail.com",
        password: "123456",
        telefono: 2901654321,
        direccionEnvio: "Magallanes 9410",
        idProductosHistorial: [],
        registrado: false
    }, {
        idUsuario: 3,
        soyAdmin: false,
        usuario: "trini",
        nombreApellido: "Trini B",
        mail: "trini@gmail.com",
        password: "123456",
        telefono: 2901654321,
        direccionEnvio: "Magallanes 9410",
        idProductosHistorial: [],
        registrado: true
    }, {
        idUsuario: 4,
        soyAdmin: false,
        usuario: "pedro",
        nombreApellido: "Pedro B",
        mail: "pedro@gmail.com",
        password: "123456",
        telefono: 2901654321,
        direccionEnvio: "Güemes 150",
        idProductosHistorial: [],
        registrado: false
    }
]; */

/* function iniciarSesion(peticion, respuesta) {
    for (const usuario of usuarios) {
        if (usuario.usuario === peticion.body.usuario && usuario.password === peticion.body.password) {
            usuario.registrado = true;
            let mensaje = ` ${usuario.nombreApellido} se ha registrado exitosamente`;
            return respuesta.status(200).send({
                status: mensaje,
                token: peticion.token,
            });
        }
    }
    let mensaje = `El ${peticion.body.usuario} no existe o la contraseña es incorrecta`;
    respuesta.status(404).json(mensaje)
};
*/

async function iniciarSesion(peticion, respuesta) {
    try {
        const u = await Usuarios.findOne({ usuario: peticion.body.usuario });
        if (u.password === encriptacion(peticion.body.password)) {
            //const token = jwt.sign({ usuario: u.usuario }, u.password);
            u.logueado = true;
            await u.save();
            //console.log(u);
            respuesta.status(200).send({
                Usuario: u.usuario,
                Token: peticion.token,
            })
        } else {
            respuesta.status(400).json(`Contraseña invalida`);
        }
    } catch {
        respuesta.status(400).json(`Error al iniciar sesión`);
    }
};



/*function mostrarUsuarios(peticion, respuesta) {
    respuesta.json(usuarios);
    console.log(usuarios)
};*/

async function mostrarUsuarios(peticion, respuesta) {
    await Usuarios.find({}).then((usuarios) => {
        return respuesta.status(200).json(usuarios);
    })
};

/* function mostrarUsuariosHistorial(peticion, respuesta) {
    let historialUsuarioID;
    for (const usuario of usuarios) {
        if (Number(peticion.headers.id) === usuario.idUsuario) {
            historialUsuarioID = usuario.idProductosHistorial;
            return respuesta.status(200).json(historialUsuarioID);
        }
    }
    respuesta.status(404).json('No encontro usuario.');
}; */

/* async function mostrarUsuariosHistorial(peticion, respuesta) {
    try {
        const usuarioID = Number(peticion.params.id);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        const historialUsuario = usuarioBD.idProductosHistorial;
        await respuesta.status(200).json(historialUsuario)
    }
    catch {
        (e) => {
            let mensaje = `El usuario no ha sido encontrado.`;
            return respuesta.status(404).json(mensaje);
        }
    }
}; */

async function mostrarUsuariosHistorial(peticion, respuesta) {
    const usuarioID = Number(peticion.params.id);
    await Usuarios.findOne({ idUsuario: usuarioID }).then((usuarioBD) => {
        const historialUsuario = usuarioBD.idProductosHistorial;
        respuesta.status(200).json(historialUsuario);
    })
        .catch((e) => {
            let mensaje = `El usuario no ha sido encontrado.`;
            return respuesta.status(404).json(mensaje);
        })
};

async function nuevoUsuario(peticion, respuesta) {
    const usuarios = await Usuarios.find();
    let nuevoUsuario = new Usuarios();
    const password = peticion.body.password;
    nuevoUsuario.idUsuario = usuarios[usuarios.length - 1].idUsuario + 1; //ACTUALIZAR
    nuevoUsuario.usuario = peticion.body.usuario;
    nuevoUsuario.nombreApellido = peticion.body.nombreApellido;
    nuevoUsuario.mail = peticion.body.mail;
    nuevoUsuario.password = encriptacion(password);
    nuevoUsuario.telefono = peticion.body.telefono;
    nuevoUsuario.agendaDirecciones = [{
        idDireccion: 1,
        direccion: peticion.body.agendaDirecciones.direccion,
        detalle: peticion.body.agendaDirecciones.detalle
    }]; //chequear esto!!
    await nuevoUsuario.save().then((x) => {
        let mensaje = `El usuario ${nuevoUsuario.nombreApellido} ha sido registrado con éxito`;
        respuesta.status(200).json(mensaje);
    })
        .catch((e) => {
            console.log(e);
            let mensaje = `El ${nuevoUsuario.usuario} o el ${nuevoUsuario.mail} ya ha sido registrado. Recupere su cuenta o intente con otro correo electrónico.`;
            respuesta.status(404).json(mensaje)
        })
};

async function agregarDireccion(peticion, respuesta) {
    try {
        const usuarioID = Number(peticion.headers.id);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        const nuevaDireccion = {
            idDireccion: Number(peticion.body.idDireccion),
            direccion: peticion.body.direccion,
            detalle: peticion.body.detalle
        };
        await usuarioBD.agendaDirecciones.push(nuevaDireccion);
        await usuarioBD.save();
        let mensaje = `La nueva dirección se ha agregado con éxito`;
        await respuesta.status(200).json(mensaje);
    } catch {
        (e) => {
            console.log(e);
            let mensaje = `Complete los campos e intente nuevamente`;
            respuesta.status(400).json(mensaje);
        }
    }

};

function adminUsuario(peticion, respuesta) {
    let usuarioID = Number(peticion.params.id);
    for (const usuario of usuarios) {
        if (usuarioID == usuario.idUsuario) {
            usuario.soyAdmin = true;
            let mensaje = `El usuario ${usuario.nombreApellido} ahora es Administrador`
            return respuesta.status(200).json(mensaje);
        }
    }
    let mensaje = `No se ha encontrado usuario`;
    return respuesta.status(404).json(mensaje);
}; //Al pedo!!

/*function borrarUsuario(peticion, respuesta) {
    const idUsuarioBorrado = Number(peticion.params.id);
    for (const usuario of usuarios) {
        const posicionUsuario = usuarios.indexOf(usuario);
        if (idUsuarioBorrado === usuario.idUsuario) {
            let mensaje = `Usuario ${usuario.nombreApellido} ha sido eliminado.`;
            usuarios.splice(posicionUsuario, 1);
            return respuesta.status(200).json(mensaje);
        }
    }
    let mensaje = `El usuario no ha sido encontrado.`;
    respuesta.status(404).json(mensaje);
};*/

async function borrarUsuario(peticion, respuesta) {
    const idUsuarioBorrado = Number(peticion.params.id);
    await Usuarios.findOneAndDelete({ idUsuario: idUsuarioBorrado }).then((usuarioBorrado) => {
        let mensaje = `Usuario ${usuarioBorrado.nombreApellido} ha sido eliminado.`;
        return respuesta.status(200).json(mensaje);
    })
        .catch((e) => {
            let mensaje = `El usuario no ha sido encontrado.`;
            return respuesta.status(404).json(mensaje);
        })
}; // CAMBIAR findOneByIdAndDelete( const: _id ) 

async function suspenderUsuario(peticion, respuesta) {
    try {
        const usuarioID = Number(peticion.params.id);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        if (usuarioBD.suspendido === false) {
            usuarioBD.suspendido = true;
            await usuarioBD.save();
            let mensaje = ` ${usuarioBD.nombreApellido} ha sido suspendido`;
            return respuesta.status(200).json(mensaje);
        } else {
            usuarioBD.suspendido = false;
            await usuarioBD.save();
            let mensaje = ` ${usuarioBD.nombreApellido} dejó de estar suspendido`;
            return respuesta.status(201).json(mensaje);

        }
    } catch {
        (e) => {
            console.log(e);
        }
    }
};

module.exports = {
    iniciarSesion,
    nuevoUsuario,
    mostrarUsuarios,
    mostrarUsuariosHistorial,
    adminUsuario,
    borrarUsuario,
    suspenderUsuario,
    agregarDireccion
};