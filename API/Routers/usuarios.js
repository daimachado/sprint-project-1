const express = require('express');

const { mostrarUsuarios, nuevoUsuario, borrarUsuario, mostrarUsuariosHistorial, adminUsuario, suspenderUsuario, agregarDireccion } = require('../Funciones/usuarios');
const { usuarioAdmin, usuarioSuspendido, autorizacion, usuarioLogueado, camposNuevoUsuario } = require('../Middleware/delservidor');

function routersUsuarios() {
    const router = express.Router();
    router.get('/usuarios', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, mostrarUsuarios);
    router.post('/usuarios',camposNuevoUsuario, nuevoUsuario);
    ///router.put('/usuarios/:id', usuarioSuspendido, usuarioAdmin, adminUsuario); // no b
    router.post('/usuarios/:id', autorizacion, usuarioLogueado, usuarioSuspendido, agregarDireccion); 
    router.put('/usuarios/:id', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, suspenderUsuario); 
    router.delete('/usuarios/:id', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, borrarUsuario);
    router.get('/usuarios/:id', autorizacion, usuarioLogueado, usuarioSuspendido, mostrarUsuariosHistorial);
    return router;
};

module.exports = {
    routersUsuarios
};