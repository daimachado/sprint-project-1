const express = require('express');
const { mostrarMetodosPago, nuevoMetodoPago, editarMetodosPago, borrarMetodoPago } = require('../Funciones/metodosPago');
const { usuarioAdmin, usuarioSuspendido, autorizacion, usuarioLogueado } = require('../Middleware/delservidor');

function routersMetodosPago() {
    const router = express.Router();
    router.get('/metodosPago', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, mostrarMetodosPago);
    router.post('/metodosPago', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, nuevoMetodoPago);
    router.put('/metodosPago/:id', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, editarMetodosPago);
    router.delete('/metodosPago/:id', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, borrarMetodoPago);
    return router
}

module.exports = {
    routersMetodosPago
}