const { Usuarios } = require("../Database/Models/usuarios");
const { Pedidos } = require("../Database/Models/pedidos");
const jwt = require('jsonwebtoken');
const { create } = require("domain");
const { encriptacion } = require("./encriptacion");
const { MetodosPago } = require("../Database/Models/metodosPago");
const {config} = require ('dotenv');


//Messirve pal inicio de sesión

async function autenticacion(peticion, respuesta, next) {
    const { JWT_SECRET } = process.env;
    const { usuario, password } = peticion.body;
    const usuarioAca = Usuarios.findOne({
        where: {
            usuario,
            password: encriptacion(password),
        },
    })
    if (usuarioAca) {
        peticion.token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + 300,
            soyAdmin: usuarioAca.soyAdmin,
            usuario: usuarioAca.usuario,
            nombreApellido: usuarioAca.nombreApellido,
            mail: usuarioAca.mail,
            telefono: usuarioAca.telefono,
            agendaDirecciones: usuarioAca.agendaDirecciones,
            idProductosHistorial: usuarioAca.idProductosHistorial,
            logueado: usuarioAca.logueado,
            suspendido: usuarioAca.suspendido
        }, JWT_SECRET);
        next();
    } else {
        respuesta.status(403).send('Credenciales inválidas')
    }
};

function autorizacion(peticion, respuesta, next) {
    const { JWT_SECRET } = process.env;
    const authHeader = peticion.headers.authorization || '';
    const token = authHeader.replace('Bearer ', '');
    jwt.verify(token, JWT_SECRET, (err, decoded) => {
        if (err) {
            console.log(err);
            respuesta.status(401).send('Debe iniciar sesión para poder continuar...');
        } else {
            peticion.usuario = decoded;
            next();
        }
    });

};

//NO HIZO FALTA!!
function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};


/* function usuarioRegistrado(peticion, respuesta, next) {
    const usuarioRegistrado = Number(peticion.headers.id);
    for (const usuario of usuarios) {
        if (usuarioRegistrado === usuario.idUsuario) {
            if (usuario.registrado) {
                return next();
            }
        }
    }
    let mensaje = `Debe iniciar sesión para realizar las siguientes acciones.`;
    return respuesta.status(404).json(mensaje);

}; */

/* function usuarioAdmin(peticion, respuesta, next) {
    const adminUsuario = Number(peticion.headers.id);
    for (const usuario of usuarios) {
        if (adminUsuario === usuario.idUsuario) {
            if (usuario.soyAdmin) {
               return next();
            }
        }
    }
    let mensaje = `Acceso denegado`;
    return respuesta.status(403).json(mensaje);
}; */

async function usuarioAdmin(peticion, respuesta, next) {
    try {
        const adminUsuario = Number(peticion.headers.id);
        const adminBD = await Usuarios.findOne({ idUsuario: adminUsuario })
        if (adminBD.soyAdmin) {
            return next();
        } else {
            let mensaje = `Acceso denegado`;
            return respuesta.status(403).json(mensaje);
        }
    } catch {
        (e) => {
            let mensaje = `Error del sistema.`
            return respuesta.status(500).json(mensaje)
        }
    }
};

/* function mailDuplicado(peticion, respuesta, next) {
    let mailUsuario = peticion.body.mail;
    for (const usuario of usuarios) {
        if (mailUsuario === usuario.mail) {
            let mensaje = `El ${mailUsuario} ya ha sido registrado. Recupere su cuenta o intente con otro correo electrónico.`;
            return respuesta.status(404).json(mensaje);
        }
    }
    return next();
}; */

async function seleccionarMedioPago(peticion, respuesta, next) {
    try {
        //console.log("pasa medioPago");
        const pedidoID = Number(peticion.params.id);
        const metodoPagoID = Number(peticion.body.idPago);
        const pedidoBD = await Pedidos.findOne({ idPedido: pedidoID });
        const metodoPagoBD = await MetodosPago.findOne({ idPago: metodoPagoID });
        if (metodoPagoBD.activo) {
            //pedidoBD.medioPago.push(metodoPagoBD);
            pedidoBD.medioPago = metodoPagoBD;
            await pedidoBD.save();
            //console.log(pedidoBD.medioPago);
            //let mensaje = `Usted ha seleccionado el siguiente método de pago: ${pago.metodoPago}.`;
            //console.log("ultimo mediopago");
            return await next();
        } else {
            let mensaje = `El método de pago seleccionado no se encuentra activo en estos momentos. Por favor intente nuevamente con otro...`;
            respuesta.status(404).json(mensaje);
        }
    }
    catch {
        (e) => {
            let mensaje = `Debe seleccionar un método de pago para continuar. Intente nuevamente.`;
            respuesta.status(402).json(mensaje);
        }
    }
};


/* function seleccionarMedioPago(peticion, respuesta, next) {
    const pedidoID = Number(peticion.params.id);
    const metodoPagoID = Number(peticion.body.idPago);
    for (const pedido of pedidos) {
        if (pedidoID === pedido.idPedido) {
            for (const pago of metodosPago) {
                if (metodoPagoID === pago.idPago) {
                    if (pago.activo === true) {
                        pedido.medioPago.push(pago);
                        //let mensaje = `Usted ha seleccionado el siguiente método de pago: ${pago.metodoPago}.`;
                        return next();
                    } else {
                        let mensaje = `El método de pago seleccionado no se encuentra activo en estos momentos. Por favor intente nuevamente con otro...`;
                        respuesta.status(404).json(mensaje);
                    }
                }
            }
        }
    }
    let mensaje = `Debe seleccionar un método de pago para continuar. Intente nuevamente.`;
    respuesta.status(402).json(mensaje);
}; */

/* function direccionEnvio(peticion, respuesta, next) {
    let usuarioID = Number(peticion.headers.id);
    let pedidoID = Number(peticion.params.id);
    for (const usuario of usuarios) {
        if (usuarioID === usuario.idUsuario) {
            for (const pedido of pedidos) {
                if (pedidoID === pedido.idPedido) {
                    if (peticion.body.direccionEnvioPedido) {
                        pedido.direccionEnvioPedido = peticion.body.direccionEnvioPedido;
                        //let mensaje = `El envío se remitirá a la dirección que escribió.`
                        //respuesta.status(200).json(mensaje);
                    } else {
                        pedido.direccionEnvioPedido = usuario.direccionEnvio;
                        //let mensaje = `El envío se remitirá a su dirección registrada en la cuenta.`
                        //respuesta.status(200).json(mensaje);

                    }

                }
            }
        }
    }
    next();
}; */

async function direccionEnvio(peticion, respuesta, next) {
    try {
        //console.log("pasa por dire envio");
        const usuarioID = Number(peticion.headers.id);
        const pedidoID = Number(peticion.params.id);
        const direccionID = Number(peticion.body.idDireccion);
        const direccion = peticion.body.direccion;
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        const pedidoBD = await Pedidos.findOne({ idPedido: pedidoID });
        //        if (peticion.body.idDireccion !== null) {
        for (const dire of usuarioBD.agendaDirecciones) {
            //console.log("pasa por dire envio2");
            if (direccionID === Number(dire.idDireccion)) {
                pedidoBD.direccionEnvioPedido = dire.direccion;
                await pedidoBD.save();
            }
            //            }
            else {
                pedidoBD.direccionEnvioPedido = direccion;
                await pedidoBD.save();
            }
        }
        
         return next();
    } catch {
        (e) => {
            console.log(e);
            let mensaje = "Debe seleccionar una dirección para el destino de su pedido";
            return respuesta.status(404).json(mensaje)
        }
    }
}; // Un if para que me seleccione si o si una dire.

/* async function camposNuevoUsuario(peticion, respuesta, next) {
    if (nuevoUsuario.usuario && nuevoUsuario.nombreApellido && nuevoUsuario.mail && nuevoUsuario.password && nuevoUsuario.telefono && nuevoUsuario.agendaDirecciones) {
        return await next();
    } else {
        let mensaje = `Para registrarse debe completar todos los campos.`;
        return respuesta.status(401).json(mensaje);
    }
}; */

async function camposNuevoUsuario(peticion, respuesta, next) {
    try {
        const a = peticion.body.usuario;
        const b = peticion.body.nombreApellido;
        const c = peticion.body.mail;
        const d = peticion.body.password;
        const e = peticion.body.telefono;
        const f = peticion.body.agendaDirecciones
        if (a && b && c && d && e && f) {
            return await next();
        } else {
            let mensaje = `Para registrarse debe completar todos los campos.`;
            return respuesta.status(401).json(mensaje);
        }
    } catch {
        (e) => {
            let mensaje = `Error del sistema.`
            return respuesta.status(500).json(mensaje)
        }
    }
};



async function pedidoAbierto(peticion, respuesta, next) {
    try {
        const pedidoID = Number(peticion.params.id);
        const pedidoBD = await Pedidos.findOne({ idPedido: pedidoID });
        if (pedidoBD.cerrado === false) {
            console.log("por pasar por pedido abierto");
            return next();
        } else {
            let mensaje = `Ya no es posible modificar su pedido... Realice un nuevo pedido o comuniquese con nuestro operador.`
            return respuesta.status(406).json(mensaje)
        }
    } catch {
        (e) => {
            let mensaje = `Error del sistema.`
            return respuesta.status(500).json(mensaje)
        }
    }
};

async function usuarioSuspendido(peticion, respuesta, next) {
    try {
        const usuarioID = Number(peticion.headers.id);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID });
        if (usuarioBD.suspendido === false) {
            return next();
        } else {
            let mensaje = `Se encuentra suspendido... Comuniquese con nuestro operador.`
            respuesta.status(406).json(mensaje)
        }
    } catch {
        (e) => {
            let mensaje = `Error del sistema.`
            return respuesta.status(500).json(mensaje)
        }
    }
};

async function usuarioLogueado(peticion, respuesta, next) {
    try {
        const usuarioID = Number(peticion.headers.id);
        const usuarioBD = await Usuarios.findOne({ idUsuario: usuarioID })
       console.log(usuarioBD.logueado);
        if (usuarioBD.logueado) {
            return next();
        } else {
            let mensaje = `Debe iniciar sesión para poder continuar!!`;
            return respuesta.status(403).json(mensaje);
        }
    } catch {
        (e) => {
            let mensaje = `Error del sistema.`
            return respuesta.status(500).json(mensaje)
        }
    }
};


module.exports = {
    usuarioAdmin,
    seleccionarMedioPago,
    direccionEnvio,
    camposNuevoUsuario,
    pedidoAbierto,
    usuarioSuspendido,
    autenticacion,
    autorizacion,
    usuarioLogueado
}