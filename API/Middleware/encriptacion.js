const { createHmac } = require('crypto');

function encriptacion(secreto) {
    return createHmac('sha256', secreto).digest('hex');
};

module.exports = {
    
    encriptacion

}